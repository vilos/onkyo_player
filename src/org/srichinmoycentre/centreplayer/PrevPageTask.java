
package org.srichinmoycentre.centreplayer;

import java.util.ArrayList;
import java.util.List;

import org.srichinmoycentre.centreplayer.CentrePlayer.eSection;

public class PrevPageTask extends AbstractTask {

  private int mCursorIndex;
  private List<String> mItems;
  private boolean mGoToZero;


  public PrevPageTask() {
    mGoToZero = false;
    mItems = new ArrayList<String>();
    mConnection.sendCommand("NTCUP");
  }


  private void end() {
    mPlayer.changeSection(eSection.FOLDER, mItems.toArray(new String[]{}));
  }


  @Override
  public boolean processCommand(String command) {
    
    if (command.startsWith("NLSU")) {
      int index = Integer.valueOf(command.substring(4, 5));
      String name = command.substring(6);

      // set length of the array so that it matches the position of the item to be added to the end
      while (mItems.size() > index) {
        mItems.remove(mItems.size() - 1);
      }
      while (mItems.size() < index) {
        mItems.add("");
      }

      mItems.add(name);

      // Here we assume the page index was above zero and so it can't wrap to the last page (we don't even know how many
      // pages there are). The wrapping is detected in the main command processor (but only for forward page scrolling),
      // so we return true to prevent it from processing NLSU.
      return true;
    }

    if (command.startsWith("NLSC")) {
      if (command.charAt(4) == '-') {
        mCursorIndex = -1;
      } else {
        mCursorIndex = Integer.valueOf(command.substring(4, 5));
      }

      if (mGoToZero) {
        if (mCursorIndex == 0) {
          end();
        } else {
          mConnection.sendCommand("NTCUP");
        }
      } else if (mCursorIndex == 0 || command.charAt(5) == 'P') { // new page detected
        if (mCursorIndex > 0) {
          mGoToZero = true;
          mConnection.sendCommand("NTCUP");
        } else {
          end();
        }
      } else { // scroll up to scroll the page
        mConnection.sendCommand("NTCUP");
      }
      return true;
    }

    return false;
  }

}
