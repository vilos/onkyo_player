
package org.srichinmoycentre.centreplayer;

import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;

import android.os.AsyncTask;

public class OnkyoConnector {

  static String RECEIVER_IP = "192.168.8.71";
  //static String RECEIVER_IP = "10.0.2.2";

  private Socket mSocket;


  public void terminate() {
    try {
      mSocket.shutdownOutput();
    } catch (Exception ex) {}
    try {
      mSocket.close();
    } catch (Exception ex) {}
    mSocket = null;
    CentrePlayer.Log("Connection terminated");
  }


  public void connect() {
    try {
      if (mSocket != null) {
        terminate();
      }
      CentrePlayer.Log("Connecting to " + RECEIVER_IP);
      mSocket = new Socket();
      SocketAddress address = new InetSocketAddress(RECEIVER_IP, 60128);
      mSocket.connect(address, 2000);
      new ListenTask().execute();
      CentrePlayer.Log("Socket connected");
    } catch (Exception e) {
      e.printStackTrace();
      CentrePlayer.sInstance.onConnectionError();
    }
  }


  public void sendCommand(String command) {
    if (mSocket == null)
      return;

    try {
      CentrePlayer.Log("OUT: " + command);
      byte[] packet = commandToPacket(command);
      mSocket.getOutputStream().write(packet);
      mSocket.getOutputStream().flush();
    } catch (Exception ex) {
      ex.printStackTrace();
      CentrePlayer.sInstance.onConnectionError();
    }
  }


  private class ListenTask extends AsyncTask<Integer, Integer, Integer> {
    @Override
    protected Integer doInBackground(Integer... params) {
      try {

        CentrePlayer.Log("Listening...");
        InputStream input = mSocket.getInputStream();

        while (true) {

          StringBuffer response = new StringBuffer();
          int c = -1;
          int lastC = -1;
          while ((c = input.read()) != 10 || lastC != 13) {
            if (c == -1) {
              CentrePlayer.Log("ERR: End of input stream");
              CentrePlayer.sInstance.onConnectionError();
              return 0;
            }
            response.append((char)c);
            lastC = c;
          }

          String command = packetToCommand(response);
          if (command == null) {
            CentrePlayer.Log("ERR: Malformed response: " + response.toString());
          } else if (command.length() > 0) {
            CentrePlayer.Log("IN:  " + command);
            CentrePlayer.sInstance.onCommand(command);
          }
        }
      } catch (Exception ex) {
        ex.printStackTrace();
        CentrePlayer.sInstance.onConnectionError();
      }

      return 0;
    }
  }


  private static byte[] commandToPacket(String command) {
    command = "!1" + command;

    byte[] buffer = new byte[command.length() + 16 + 3];
    buffer[0] = 'I';
    buffer[1] = 'S';
    buffer[2] = 'C';
    buffer[3] = 'P';
    buffer[4] = 0x00;
    buffer[5] = 0x00;
    buffer[6] = 0x00;
    buffer[7] = 0x10;
    buffer[8] = 0x00;
    buffer[9] = 0x00;
    buffer[10] = 0x00;
    buffer[11] = (byte)(command.length() + 3);
    buffer[12] = 0x01;
    buffer[13] = 0x00;
    buffer[14] = 0x00;
    buffer[15] = 0x00;
    for (int i = 0; i < command.length(); i++) {
      buffer[16 + i] = (byte)command.charAt(i);
    }
    buffer[16 + command.length()] = 10;
    buffer[17 + command.length()] = 13;
    buffer[18 + command.length()] = 10;

    return buffer;
  }


  private static String packetToCommand(StringBuffer packetBuffer) {
    byte[] packet = packetBuffer.toString().getBytes();
    if (packet.length <= 17) {
      return null;
    } else {
      int dataLen = (((packet[10] & 0xff) << 8) | (packet[11] & 0xff)) - 3;
      //System.out.println(packetBuffer.length()+" "+dataLen);
      //for (int i=0; i<packet.length; i++) System.out.print((int)packet[i]+" ");
      //System.out.println();
      //for (int i=0; i<packet.length; i++) System.out.print((char)packet[i]+" ");
      //System.out.println();

      // heuristics: if it's longer then 128 bytes then it's probably album art -> get rid of it
      //if (dataLen > 128)
      //  return "";

      if (16 + dataLen > packet.length)
        return null; // something's wrong with the data length -> it points behind the packet buffer
      
      StringBuffer data = new StringBuffer();
      for (int i = 2; i < dataLen; i++) {
        int c = packet[16 + i];
        if (c == 0)
          break;
        data.append((char)c);
      }
      return data.toString();
    }
  }
}
