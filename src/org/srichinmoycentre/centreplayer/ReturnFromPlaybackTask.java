
package org.srichinmoycentre.centreplayer;

import org.srichinmoycentre.centreplayer.CentrePlayer.eSection;

public class ReturnFromPlaybackTask extends AbstractTask {

  private eSection mTargetSection;


  public ReturnFromPlaybackTask(eSection targetSection, boolean stopped) {
    mTargetSection = targetSection;
    if (stopped) {
      mConnection.sendCommand("NSTQSTN");
    } else {
      mConnection.sendCommand("NTCSTOP");
    }
  }


  @Override
  public boolean processCommand(String command) {
    if (command.startsWith("NST")) { // play status update
      if (command.charAt(3) != 'P') {
        mPlayer.changeSection(mTargetSection);
        if (mTargetSection == eSection.FOLDER) {
          mConnection.sendCommand("NTCRETURN");
        }
      }
    }

    return false;
  }

}
