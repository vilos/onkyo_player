
package org.srichinmoycentre.centreplayer;

import org.srichinmoycentre.centreplayer.CentrePlayer.eSection;

public class ResetCursorTask extends AbstractTask {

  private eSection mSection;
  private String[] mOriginalItems;
  

  public ResetCursorTask(eSection section, String[] originalItems) {
    mSection = section;
    mOriginalItems = originalItems;
    mConnection.sendCommand("NTCUP");
  }


  @Override
  public boolean processCommand(String command) {
    if (command.startsWith("NLSC")) {
      int index = -1;
      if (command.charAt(4) != '-') {
        index = Integer.valueOf(command.substring(4, 5));
      }

      if (index == 0) {
        mPlayer.changeSection(mSection, mOriginalItems);
      } else {
        mConnection.sendCommand("NTCUP");
      }

      return true;
    }

    return false;
  }

}
