
package org.srichinmoycentre.centreplayer;

import org.srichinmoycentre.centreplayer.CentrePlayer.eSection;

public class RunPreparedTask extends AbstractTask {

  private boolean mCanDive;
  private int mPositionCounter;
  private PreparedItem mPreparedItem;
  private String mInputType;
  private boolean mCalledTop;


  public RunPreparedTask(PreparedItem preparedItem, String inputType) {
    mPreparedItem = preparedItem;
    mPositionCounter = 0;
    mCanDive = false;
    mCalledTop = false;
    mInputType = inputType;
    if (mInputType.compareTo(mPlayer.mInputMethod) == 0) {
      mConnection.sendCommand("NTCTOP"); // should go to the disc root menu (or DLNA root menu, but onCommand will detect that and jump to the disc menu)
      mCalledTop = true;
    } else {
      mConnection.sendCommand("SLI" + mInputType);
    }
  }


  @Override
  public boolean processCommand(String command) {

    if (!mCalledTop && command.compareTo("SLI" + mInputType) == 0) {
      mConnection.sendCommand("NTCTOP");
      mCalledTop = true;
    }

    if (command.compareTo("NLSU0-MyBookLive-Twonky") == 0) { // open the disc if we're in the DLNA menu (e.g. after NTCTOP or NSV00)
      mConnection.sendCommand("NLSL0");
      return true;
    }

    if (command.compareTo("NLSU0-Music") == 0) { // open the MUSIC submenu when in root menu of the disc
      mConnection.sendCommand("NLSL0");
      return true;
    }

    if (command.compareTo("NLSU9-Genre/Song") == 0) { // open the FOLDER submenu when in menu MUSIC
      mPlayer.mNestingLevel = 0;
      mCanDive = true; // now we can dive deep into the folder structure
      mConnection.sendCommand("NLSL6"); // note 6 here and not 9
      return true;
    }

    if (command.compareTo("NLSU0-No Item") == 0) { // there is no data inserted
      mPlayer.changeSection(eSection.PREPARED);
      DialogHelper.showError(CentrePlayer.getResString(R.string.error_no_data));
    }

    if (command.startsWith("NLSU0-") && mCalledTop && mInputType.compareTo("29") == 0) { // we're in the usb root menu
      mPlayer.mNestingLevel = 0;
      mCanDive = true; // now we can dive deep into the folder structure
      mConnection.sendCommand("NLSL0");
    }

    if (command.startsWith("NLSC")) {
      if (mCanDive) { // run prepared item
        if (command.charAt(4) != '-') { // we just got one level deeper
          int cursorPos = Integer.valueOf(command.substring(4, 5));
          if (cursorPos != mPositionCounter % 10) {
            System.out.println("WARNING: position counter (" + mPositionCounter + ") doesn't match cursor position ("
                + cursorPos + ")");
          }
        }
        //System.out.println(mPositionCounter+" "+mPlayer.mNestingLevel+" "+mPreparedItem.path.get(mPlayer.mNestingLevel)+" "+mPreparedItem.path.size());
        if (mPositionCounter < mPreparedItem.path.get(mPlayer.mNestingLevel)) { // need to scroll down to the item
          mConnection.sendCommand("NTCDOWN");
          mPositionCounter++;
        } else if (mPlayer.mNestingLevel == mPreparedItem.path.size() - 1) { // this is the last level of the path, we're finished!
          mPlayer.changeSection(eSection.FOLDER);
          mPositionCounter = 0;
          mPlayer.mNestingLevel++;
          mConnection.sendCommand("NTCSELECT");
        } else { // go one more level deeper in the folder structure
          mPositionCounter = 0;
          mPlayer.mNestingLevel++;
          mConnection.sendCommand("NTCSELECT");
        }
      }
    }

    return false;
  }

}
