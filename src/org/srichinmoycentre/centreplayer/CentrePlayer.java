
package org.srichinmoycentre.centreplayer;

import java.util.ArrayList;
import java.util.List;

import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.view.*;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.*;

public class CentrePlayer extends ListActivity {

  public static final int SCREEN_TIMEOUT = 7200000; // milliseconds


  public enum eSection {
    INVALID, CONNECTING, PROCESSING, ERROR, MAIN, PREPARED, FOLDER, PLAYING
  }


  public static CentrePlayer sInstance;
  public MenuAdapter mMenuAdapter; // provides content for the list box
  public eSection mCurrentSection;
  public int mCurrentContentView; // list box, player, ...
  public OnkyoConnector mConnection;
  public int mPageIndex; // current page
  public List<Integer> mPath; // path to the current item (submenu or song)
  public List<PreparedItem> mPreparedList;
  public int mPreparedCurrentItem;
  public AbstractTask mTask; // currently processing task (or null)
  public String mFirstPageItem; // first item of the first page; used to determine we've returned to the first page
  public int mNestingLevel; // how deep we are in the directory structure
  public boolean mFirstTimeInMain;
  public boolean mTerminating; // the application is shutting down
  public int mDefaultTimeout;
  public String mLastCommand; // last command processed
  public String mInputMethod; // input method of the receiver (NET, USB, ...)


  private void onConnectionErrorImpl() {
    if (!mTerminating) {
      mConnection.terminate();
      changeSection(eSection.ERROR);
      DialogHelper.showError(getResString(R.string.error_connecting));
    }
    mTask = null;
  }


  private void onCommandImpl(String command) {

    if (mTask != null && mTask.processCommand(command)) {
      return; // processed by the task
    }

    if (command.compareTo("PWR01") == 0 && mCurrentSection == eSection.CONNECTING) {
      changeSection(eSection.MAIN);
    }

    if (command.compareTo("PWRN/A") == 0 || command.compareTo("PWR00") == 0) {
      onConnectionErrorImpl(); // turned off
    }

    // input method
    if (command.startsWith("SLI")) {
      mInputMethod = command.substring(3);
    }

    if (command.compareTo("NLSU4-DLNA") == 0) { // jump to DLNA if we're in the NET menu
      mConnection.sendCommand("NSV00");
    }

    if (mCurrentSection == eSection.MAIN
        && (command.contains("DLNA") || command.contains("MyBookLive") || command.contains("Photos"))) { // wrong commands outside of MAIN
      changeSection(eSection.MAIN);
      mTask = null;
      if (!mFirstTimeInMain) {
        DialogHelper.showToast(getResString(R.string.error_unexpected_response));
      }
      return;
    }

    // jacket art
    if (mCurrentSection == eSection.PLAYING && command.startsWith("NJA") && !mLastCommand.startsWith("NJA")) {
      DialogHelper.showToast(getResString(R.string.warning_jacket_art));
    }

    // cursor update
    if (command.startsWith("NLSC")) {
      int index = -1;
      if (command.charAt(4) != '-') {
        index = Integer.valueOf(command.substring(4, 5));
      }
      if (index > 0 && mTask == null) {
        mTask = new ResetCursorTask(mCurrentSection, mMenuAdapter.getItems().toArray(new String[]{}));
      }
    }

    // page item
    if (command.startsWith("NLSU")) {
      int index = Integer.valueOf(command.substring(4, 5));
      String name = command.substring(6);
      if (mCurrentSection == eSection.FOLDER && mPreparedCurrentItem != -1) { // we're processing the prepared list AND we have already reached the target
        changeSection(eSection.PREPARED); // the target seems not to be a track, but a folder instead -> error
        DialogHelper.showToast(getResString(R.string.error_prepared_not_track));
      } else if (mCurrentSection == eSection.FOLDER) {
        if (index == 0) { // prepare for new page
          mMenuAdapter.clear();
          if (mPageIndex == 0) { // it's the first page, so store the first item, so that we can detect the page later
            mFirstPageItem = name;
          } else if (name.compareTo(mFirstPageItem) == 0) { // this will happen after scrolling behind the last item of the list
            mPageIndex = 0;
          }
        }
        mMenuAdapter.setLength(index);
        mMenuAdapter.add(name);
      }
    }

    // track info
    if (command.startsWith("NTI")) {
      if (mCurrentSection == eSection.FOLDER || mCurrentSection == eSection.PLAYING) { // new track started playing
        if (mPreparedCurrentItem != -1
            && mPreparedList.get(mPreparedCurrentItem).title.compareTo(command.substring(3)) != 0) {
          System.out.println("WARNING: the track playing (" + command.substring(3) + ") is not what we've prepared ("
              + mPreparedList.get(mPreparedCurrentItem).title + ")");
          mConnection.sendCommand("NTCSTOP");
          changeSection(eSection.PREPARED); // the track currently playing is not the one we've prepared, probably because it has already ended; so let's return back to the menu
        } else {
          changeSection(eSection.PLAYING); // something started playing, so make sure we're displaying the player
          if (mCurrentSection == eSection.PLAYING) {
            ((TextView)findViewById(R.id.trackName)).setText(command.substring(3));
          }
        }
      }
    }

    // time info
    if (command.startsWith("NTM")) {
      if (mCurrentSection == eSection.MAIN || mCurrentSection == eSection.CONNECTING
          || mCurrentSection == eSection.PROCESSING || mCurrentSection == eSection.PREPARED) {
        mConnection.sendCommand("NTCSTOP"); // we don't want anything to be played in these sections
      } else if (mCurrentSection == eSection.FOLDER) {
        mConnection.sendCommand("NTIQSTN"); // something is playing but we don't know about it cause we didn't get NTI
      } else if (mCurrentSection == eSection.PLAYING) { // update the time
        findViewById(R.id.buttonPlay).setVisibility(View.GONE);
        findViewById(R.id.buttonStop).setVisibility(View.VISIBLE);
        ((TextView)findViewById(R.id.trackTime)).setText(command.substring(3));
      }
    }

    // play status
    if (command.startsWith("NST")) {
      if (mCurrentSection == eSection.PLAYING) {
        if (command.charAt(3) == 'P') {
          findViewById(R.id.buttonPlay).setVisibility(View.GONE);
          findViewById(R.id.buttonStop).setVisibility(View.VISIBLE);
        } else {
          findViewById(R.id.buttonPlay).setVisibility(View.VISIBLE);
          findViewById(R.id.buttonStop).setVisibility(View.GONE);
        }
      }
    }

    mLastCommand = command;

    System.out.println("Page index: " + mPageIndex);
  }


  public void onButtonPrev(View v) {
    mConnection.sendCommand("NTCTRDN");
  }


  public void onButtonNext(View v) {
    mConnection.sendCommand("NTCTRUP");
  }


  public void onButtonPlay(View v) {
    mConnection.sendCommand("NTCPLAY");
  }


  public void onButtonStop(View v) {
    mConnection.sendCommand("NTCSTOP");
  }


  public void onPrevPage(View v) {
    if (mCurrentSection == eSection.FOLDER && mPageIndex > 0) {
      // note that previous page can be scrolled only if we know there is any; we don't know how many pages there are,
      // so we wouldn't know what to set to the page index if we jumped below zero
      mPageIndex--;
      mTask = new PrevPageTask();
    }
  }


  public void onNextPage(View v) {
    if (mCurrentSection == eSection.FOLDER) {
      mPageIndex++;
      mTask = new NextPageTask(mMenuAdapter.getItems().toArray(new String[]{}));
    }
  }


  @Override
  protected void onListItemClick(ListView l, View v, int position, long id) {
    if (mTask != null) {
      return;
    }

    switch (mCurrentSection) {
      case ERROR :
        init();
        break;
      case MAIN :
        mFirstTimeInMain = false;
        switch (position) {
          case 0 :
            mTask = new GoToMusicFolderTask("2B");
            break;
          case 1 :
            mTask = new GoToMusicFolderTask("29");
            break;
          case 2 :
            if (mPreparedList.size() == 0) {
              DialogHelper.showToast(getResString(R.string.info_no_prepared));
            } else {
              changeSection(eSection.PREPARED);
            }
            break;
        }
        break;
      case FOLDER :
        mPath.add(mPageIndex * 10 + position);
        mNestingLevel++;
        mPageIndex = 0;
        mConnection.sendCommand("NLSL" + position);
        break;
      case PREPARED :
        mPreparedCurrentItem = position;
        mTask = new RunPreparedTask(mPreparedList.get(mPreparedCurrentItem), "2B");
        break;
    }
  }


  @Override
  public boolean onKeyDown(int keyCode, KeyEvent event) {
    if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {

      if (mCurrentSection == eSection.CONNECTING) {
        onConnectionErrorImpl(); // canceling connection results in connection error
        return true;
      }

      if (mTask != null) {
        changeSection(eSection.MAIN);
        mTask = null; // interrupt the current task
        return true;
      }

      switch (mCurrentSection) {
        case ERROR :
          return super.onKeyDown(keyCode, event);
        case PLAYING :
          backtrackPath();
          boolean stopped = findViewById(R.id.buttonPlay).getVisibility() == View.VISIBLE;
          if (mPreparedCurrentItem != -1) {
            mTask = new ReturnFromPlaybackTask(eSection.PREPARED, stopped);
          } else if (mNestingLevel <= 0) {
            mTask = new ReturnFromPlaybackTask(eSection.MAIN, stopped);
          } else {
            mTask = new ReturnFromPlaybackTask(eSection.FOLDER, stopped);
          }
          break;
        case FOLDER :
          backtrackPath();
          if (mPreparedCurrentItem != -1) { // we got into this folder from the prepared menu, so we need to return there
            changeSection(eSection.PREPARED);
          } else if (mNestingLevel < 0) { // jump to main menu
            changeSection(eSection.MAIN);
          } else { // jump to previous folder
            mConnection.sendCommand("NTCRETURN");
          }
          break;
        case MAIN : // quit the app
          mTerminating = true;
          mConnection.terminate();
          return super.onKeyDown(keyCode, event);
        case PREPARED :
          changeSection(eSection.MAIN);
          break;
      }
      return true;
    }

    if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
      mConnection.sendCommand("MVLDOWN");
      return true;
    }

    if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
      mConnection.sendCommand("MVLUP");
      return true;
    }

    return super.onKeyDown(keyCode, event);
  }


  private void backtrackPath() {
    if (mPath.size() > 0) {
      mPageIndex = mPath.get(mPath.size() - 1) / 10;
      mPath.remove(mPath.size() - 1);
    }
    mNestingLevel--;
  }


  public void changeSection(eSection section) {
    changeSection(section, new String[]{});
  }


  public void changeSection(eSection section, String[] items) {
    if (section == eSection.INVALID) {
      return;
    }

    if (mCurrentSection != section) {
      Log("Entering section " + section);
    }

    DialogHelper.hide();

    int contentView = R.layout.main_list;
    boolean showBottomButtons = false;
    String title = "";
    switch (section) {
      case CONNECTING :
        title = getResString(R.string.connecting);
        break;
      case PROCESSING :
        title = getResString(R.string.connecting);
        break;
      case MAIN :
        title = getResString(R.string.section_main_title);
        items = getResStringArray(R.array.section_main);
        mNestingLevel = 0;
        mPreparedCurrentItem = -1; // no prepared playing
        mPath.clear();
        mConnection.sendCommand("NTCSTOP"); // stop anything if it's playing
        break;
      case ERROR :
        title = getResString(R.string.error_connecting);
        items = new String[]{getResString(R.string.error_button)};
        break;
      case FOLDER :
        showBottomButtons = true;
        title = getResString(R.string.section_music_title);
        break;
      case PREPARED :
        title = getResString(R.string.section_prepared_title);
        items = new String[mPreparedList.size()];
        for (int i = 0; i < mPreparedList.size(); i++) {
          items[i] = mPreparedList.get(i).title;
        }
        mNestingLevel = 0;
        break;
      case PLAYING :
        contentView = R.layout.music_player;
        break;
      default :
        Log("Unknown section: " + section);
        return;
    }

    if (section != eSection.PROCESSING) {
      mTask = null;
    }

    mCurrentSection = section;

    if (mCurrentContentView != contentView) {
      mCurrentContentView = contentView;
      setContentView(contentView);
    }
    setTitle(title);
    if (mCurrentContentView == R.layout.main_list) {
      registerForContextMenu(findViewById(android.R.id.list));
      findViewById(R.id.bottomButtons).setVisibility(showBottomButtons ? View.VISIBLE : View.GONE);
      mMenuAdapter = new MenuAdapter(this, items);
      setListAdapter(mMenuAdapter);
    }
  }


  @Override
  public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
    if (mCurrentSection == eSection.FOLDER && mInputMethod.compareTo("2B") == 0) {
      AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
      menu.setHeaderTitle(mMenuAdapter.get(info.position));
      String[] menuItems = getResStringArray(R.array.context_folder);
      for (int i = 0; i < menuItems.length; i++) {
        menu.add(Menu.NONE, i, i, menuItems[i]);
      }
    } else if (mCurrentSection == eSection.PREPARED) {
      AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
      menu.setHeaderTitle(mPreparedList.get(info.position).title);
      String[] menuItems = getResStringArray(R.array.context_prepared);
      for (int i = 0; i < menuItems.length; i++) {
        menu.add(Menu.NONE, 10 + i, i, menuItems[i]);
      }
    }
  }


  @Override
  public boolean onContextItemSelected(MenuItem item) {
    AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
    switch (item.getItemId()) {
      case 0 :
        PreparedItem preparedItem = new PreparedItem();
        preparedItem.path.addAll(mPath);
        preparedItem.path.add(mPageIndex * 10 + info.position);
        preparedItem.title = mMenuAdapter.get(info.position);
        mPreparedList.add(preparedItem);
        break;
      case 10 : // remove item from prepared
        mPreparedList.remove(info.position);
        if (mPreparedList.size() == 0) {
          changeSection(eSection.MAIN);
        } else {
          changeSection(mCurrentSection);
        }
        break;
      case 11 : // remove all
        mPreparedList.clear();
        changeSection(eSection.MAIN);
        break;
    }
    return true;
  }


  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.menu_main, menu);
    return true;
  }


  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.menu_change_receiver :
        DialogHelper.showEditbox(OnkyoConnector.RECEIVER_IP, new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int whichButton) {
            OnkyoConnector.RECEIVER_IP = DialogHelper.getEditbox().getText().toString().trim();
            onConnectionError();
          }
        });
        break;
    }
    return true;
  }


  @Override
  protected void onPause() {
    System.out.println("Saving state");
    super.onPause();
    SharedPreferences preferenes = getPreferences(MODE_PRIVATE);
    SharedPreferences.Editor editor = preferenes.edit();
    editor.putInt("PreparedCount", mPreparedList.size());
    for (int i = 0; i < mPreparedList.size(); i++) {
      editor.putString("PreparedTitle" + i, mPreparedList.get(i).title);
      List<Integer> path = mPreparedList.get(i).path;
      editor.putInt("PreparedPathLength" + i, path.size());
      for (int j = 0; j < path.size(); j++) {
        editor.putInt("PreparedPath" + i + ":" + j, path.get(j));
      }
    }
    editor.putString("ReceiverIP", OnkyoConnector.RECEIVER_IP);

    editor.commit();
  }


  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    sInstance = this;
    mTerminating = false;
    mCurrentSection = eSection.INVALID;
    mCurrentContentView = -1;
    mFirstTimeInMain = true;
    mLastCommand = "";
    mInputMethod = "";
    mPreparedList = new ArrayList<PreparedItem>();
    mPath = new ArrayList<Integer>();

    mDefaultTimeout = Settings.System.getInt(getContentResolver(), Settings.System.SCREEN_OFF_TIMEOUT, SCREEN_TIMEOUT);
    Settings.System.putInt(getContentResolver(), Settings.System.SCREEN_OFF_TIMEOUT, SCREEN_TIMEOUT);
    System.out.println("Set screen timeout to " + SCREEN_TIMEOUT + ". Default was " + mDefaultTimeout + ".");

    DialogHelper.init(this);
    mConnection = new OnkyoConnector();

    SharedPreferences preferences = getPreferences(MODE_PRIVATE);
    if (preferences.getInt("PreparedCount", 0) > 0) {
      System.out.println("Loading activity from preferences.");
      int count = preferences.getInt("PreparedCount", 0);
      mPreparedList.clear();
      for (int i = 0; i < count; i++) {
        PreparedItem preparedItem = new PreparedItem();
        preparedItem.title = preferences.getString("PreparedTitle" + i, "error");
        int pathLen = preferences.getInt("PreparedPathLength" + i, 0);
        for (int j = 0; j < pathLen; j++) {
          preparedItem.path.add(preferences.getInt("PreparedPath" + i + ":" + j, 0));
        }
        mPreparedList.add(preparedItem);
      }
      OnkyoConnector.RECEIVER_IP = preferences.getString("ReceiverIP", OnkyoConnector.RECEIVER_IP);
    } else {
      System.out.println("Starting brand new activity.");
    }

    init();
  }


  @Override
  protected void onDestroy() {
    super.onDestroy();
    mConnection.terminate();
    Settings.System.putInt(getContentResolver(), Settings.System.SCREEN_OFF_TIMEOUT, mDefaultTimeout);
  }


  private void init() {
    changeSection(eSection.CONNECTING);
    new InitTask().execute();
  }


  private class InitTask extends AsyncTask<Integer, Integer, Integer> {
    @Override
    protected Integer doInBackground(Integer... params) {
      mConnection.connect();
      mConnection.sendCommand("PWR01");
      mConnection.sendCommand("PWRQSTN"); // upon receiving the response main menu is entered
      mConnection.sendCommand("SLI2B");
      return 0;
    }
  }


  public void onConnectionError() {
    runOnUiThread(new Runnable() {
      public void run() {
        onConnectionErrorImpl();
      }
    });
  }


  public void onCommand(final String command) {
    runOnUiThread(new Runnable() {
      public void run() {
        onCommandImpl(command);
      }
    });
  }


  public static String getResString(int id) {
    return sInstance.getResources().getString(id);
  }


  public static String[] getResStringArray(int id) {
    return sInstance.getResources().getStringArray(id);
  }


  public static void Log(String message) {
    System.out.println(message);
  }
}