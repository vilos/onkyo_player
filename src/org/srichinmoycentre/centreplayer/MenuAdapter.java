
package org.srichinmoycentre.centreplayer;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class MenuAdapter extends BaseAdapter {

  private List<String> mItems;
  private Context mContext;


  public MenuAdapter(Context context, String[] items) {
    mContext = context;
    mItems = new ArrayList<String>();
    for (int i = 0; i < items.length; i++) {
      mItems.add(items[i]);
    }
  }


  public void clear() {
    mItems.clear();
    notifyDataSetChanged();
  }


  public void add(String item) {
    mItems.add(item);
    notifyDataSetChanged();
  }


  public String get(int position) {
    return mItems.get(position);
  }


  public List<String> getItems() {
    return mItems;
  }


  public void setLength(int length) {
    if (mItems.size() == length) {
      return;
    }
    
    while (mItems.size() > length) {
      mItems.remove(mItems.size() - 1);
    }
    while (mItems.size() < length) {
      mItems.add("");
    }
    
    notifyDataSetChanged();
  }


  @Override
  public int getCount() {
    return mItems.size();
  }


  @Override
  public Object getItem(int position) {
    return mItems.get(position);
  }


  @Override
  public long getItemId(int position) {
    return mItems.get(position).hashCode();
  }


  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    if (convertView == null) {
      LayoutInflater vi = LayoutInflater.from(mContext);
      convertView = vi.inflate(android.R.layout.simple_list_item_1, null);
    }

    ((TextView)convertView).setText(mItems.get(position));

    return convertView;
  }

}
