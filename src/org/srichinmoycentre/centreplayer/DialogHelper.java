
package org.srichinmoycentre.centreplayer;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.EditText;
import android.widget.Toast;

public class DialogHelper {

  private static Context sContext;
  private static AlertDialog sDialog;
  private static EditText sEditbox;


  public static void init(Context context) {
    sContext = context;
  }


  public static void showInfo(String text) {
    AlertDialog.Builder builder = new AlertDialog.Builder(sContext);
    sDialog = builder.setMessage(text).show();
  }


  public static void showToast(String text) {
    hide();
    Toast.makeText(CentrePlayer.sInstance, text, Toast.LENGTH_SHORT).show();
  }


  public static void showError(String text) {
    hide();
    Toast.makeText(CentrePlayer.sInstance, text, Toast.LENGTH_SHORT).show();
  }


  public static void showEditbox(String text, DialogInterface.OnClickListener okListener) {
    hide();

    AlertDialog.Builder alert = new AlertDialog.Builder(sContext);
    sEditbox = new EditText(sContext);
    sEditbox.setText(text);
    alert.setView(sEditbox);
    alert.setPositiveButton(sContext.getResources().getString(R.string.ok), okListener);
    /*alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int whichButton) {
        dialog.cancel();
      }
    });*/

    sDialog = alert.show();
  }


  public static EditText getEditbox() {
    return sEditbox;
  }


  public static void hide() {
    if (sDialog != null) {
      sDialog.dismiss();
      sDialog = null;
    }
  }
}
