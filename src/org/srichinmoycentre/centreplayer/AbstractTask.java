package org.srichinmoycentre.centreplayer;

import org.srichinmoycentre.centreplayer.CentrePlayer.eSection;

public abstract class AbstractTask {
  
  protected CentrePlayer mPlayer;
  protected OnkyoConnector mConnection;
  
  public AbstractTask() {
    mPlayer = CentrePlayer.sInstance;
    mConnection = mPlayer.mConnection;
    mPlayer.changeSection(eSection.PROCESSING);
  }
  
  public abstract boolean processCommand(String command);
  
}
