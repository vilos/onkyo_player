
package org.srichinmoycentre.centreplayer;

import org.srichinmoycentre.centreplayer.CentrePlayer.eSection;

public class GoToMusicFolderTask extends AbstractTask {

  private String mInputType;
  private boolean mCalledTop;


  public GoToMusicFolderTask(String inputType) {
    mCalledTop = false;
    mInputType = inputType;
    if (mInputType.compareTo(mPlayer.mInputMethod) == 0) {
      mConnection.sendCommand("NTCTOP"); // should go to the disc root menu (or DLNA root menu, but onCommand will detect that and jump to the disc menu)
      if (mInputType.compareTo("29") == 0) {
        mPlayer.changeSection(eSection.FOLDER);
        mConnection.sendCommand("NTCSELECT");
      }
      mCalledTop = true;
    } else {
      mConnection.sendCommand("SLI" + mInputType);
    }
  }


  @Override
  public boolean processCommand(String command) {

    if (!mCalledTop && command.compareTo("SLI" + mInputType) == 0) {
      mConnection.sendCommand("NTCTOP");
      if (mInputType.compareTo("29") == 0) {
        mPlayer.changeSection(eSection.FOLDER);
        mConnection.sendCommand("NTCSELECT");
      }
      mCalledTop = true;
    }

    if (command.compareTo("NLSU0-MyBookLive-Twonky") == 0) { // open the disc if we're in the DLNA menu (e.g. after NTCTOP or NSV00)
      mConnection.sendCommand("NLSL0");
      return true;
    }

    if (command.compareTo("NLSU0-Music") == 0) { // open the MUSIC submenu when in root menu of the disc
      mConnection.sendCommand("NLSL0");
      return true;
    }

    if (command.compareTo("NLSU9-Genre/Song") == 0) { // open the FOLDER submenu when in menu MUSIC
      mPlayer.changeSection(eSection.FOLDER);
      mConnection.sendCommand("NLSL6"); // note 6 here and not 9
      return true;
    }

    if (command.compareTo("NLSU0-No Item") == 0) { // there is no data inserted
      mPlayer.changeSection(eSection.MAIN);
      DialogHelper.showError(CentrePlayer.getResString(R.string.error_no_data));
      return true;
    }

    /*if (command.startsWith("NLSU0-") && mInputType.compareTo("29") == 0) { // we're in the usb root menu
      mPlayer.changeSection(eSection.FOLDER);
      mConnection.sendCommand("NLSL0");
    }*/

    return false;
  }

}
