
package org.srichinmoycentre.centreplayer;

import org.srichinmoycentre.centreplayer.CentrePlayer.eSection;

public class NextPageTask extends AbstractTask {

  private int mCursorIndex;
  private String[] mOriginalItems;


  public NextPageTask(String[] originalItems) {
    mOriginalItems = originalItems;
    mConnection.sendCommand("NTCDOWN");
  }


  @Override
  public boolean processCommand(String command) {
    if (command.startsWith("NLSC")) {
      if (command.charAt(4) == '-') {
        mCursorIndex = -1;
      } else {
        mCursorIndex = Integer.valueOf(command.substring(4, 5));
      }
      
      if (mCursorIndex == 0 || command.charAt(5) == 'P') { // new page detected
        if (command.charAt(5) != 'P') { // it is a page same as the old one, otherwise we'd get P, so we must prevent mCurrentPage from incrementing
          mPlayer.mPageIndex = 0;
          mPlayer.changeSection(eSection.FOLDER, mOriginalItems);
        } else {
          mPlayer.changeSection(eSection.FOLDER);
        }
      } else { // scroll down to scroll the page
        mConnection.sendCommand("NTCDOWN");
      }
      return true;
    }

    return false;
  }

}
